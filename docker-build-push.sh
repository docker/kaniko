#!/busybox/sh

CI_REGISTRY=$1
CI_REGISTRY_USER=$2
CI_REGISTRY_PASSWORD=$3
CONTEXT=$4
DOCKERFILE=$5
REGISTRY_DESTINATION=$6

# Workaround for `kaniko should only be run inside of a container, run with the --force flag if you are sure you want to continue`
export container=docker

echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
/kaniko/executor --context "$CONTEXT" --dockerfile "$DOCKERFILE" --destination "$REGISTRY_DESTINATION"
