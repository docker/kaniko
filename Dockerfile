FROM gcr.io/kaniko-project/executor:debug

ENTRYPOINT [""]
COPY docker-build-push.sh /docker-build-push.sh
